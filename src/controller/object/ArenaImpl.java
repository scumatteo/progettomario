package controller.object;

import java.util.List;
import java.util.Optional;

import model.builder.BuilderTank;
import model.input.InputImpl;
import model.object.Proiettile;
import model.object.Tank;
import model.utility.Position2D;

public class ArenaImpl implements Arena {
	
	private static final int LIFES = 3;
	private BuilderTank builderTank;
	private Tank playerTank;
	private Tank enemyTank;

	@Override
	public void createPlayer(Position2D position, double speed) {
		this.playerTank = this.builderTank.createTank().setPosition(position).setLifes(LIFES).setSpeed(speed).build();

	}

	@Override
	public void createEnemy(Position2D position, double speed) {
		this.enemyTank = this.builderTank.createTank().setPosition(position).setLifes(LIFES).setSpeed(speed).build();
	}

	@Override
	public void setBorder(double h, double w) {
		

	}

	@Override
	public Optional<Tank> getPlayer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Optional<Tank>> getEnemies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Proiettile> getProiettile() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void shotPlayer() {
		// TODO Auto-generated method stub

	}

	@Override
	public void shotEnemy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resetAll() {
		// TODO Auto-generated method stub

	}

	@Override
	public void saveAll(String nameTxt) {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputPlayer(InputImpl i) {
		this.playerTank.update(i);
		
	}

	@Override
	public void inputEnemy(InputImpl i) {
		this.enemyTank.update(i);
		
	}

}
