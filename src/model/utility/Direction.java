package model.utility;

public enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT
}