package model.object;

import model.utility.Direction;
import model.utility.Position2D;

public class Proiettile {
	private Position2D position;
	private double angle;
	private double speed = 2.0;
	private boolean isAlive = true;
	private int nr_bounced;
	
	public Proiettile(Position2D position, double angle) {
		this.position = position;
		this.angle = angle;
	}

	public Position2D getPosition() {
		return this.position;
	}
	
	public void bounce(Direction dir) {
		if(nr_bounced>=1) {
			throw new IllegalStateException();
		}
		nr_bounced++;
		if(dir.equals(Direction.UP) || dir.equals(Direction.DOWN)){
			this.position.setX(-this.position.getX()); 
		}
		if(dir.equals(Direction.LEFT) || dir.equals(Direction.RIGHT)) {
			this.position.setY(-this.position.getY());
		}
	}
	
	public void setDead() {
		this.isAlive = false;
	}
	
	public boolean isAlive() {
		return this.isAlive;
	}
	
	public void update() {
		this.position.setX(this.position.getX()+(speed*Math.cos(Math.toRadians(angle))));
		this.position.setY(this.position.getY()+(speed*Math.sin(Math.toRadians(angle))));
	}
}
