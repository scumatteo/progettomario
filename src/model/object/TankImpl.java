package model.object;

import model.input.*;
import model.utility.*;

public class TankImpl implements Tank {
	private int lifes;
	private Position2D position;
	private double speed = 3;
	private double speedX;
	private double speedY;
	private final Cannon cannon = new Cannon();
	
	public TankImpl(Position2D position, int lifes, double speed) {
		this.position = position;
		this.lifes = lifes;
		this.speed = speed;
	}

	@Override
	public boolean isAlive() {
		return this.lifes!=0;
	}
	
	@Override
	public void setPosition(Position2D position) {
		this.position = position;
	}

	@Override
	public Position2D getPosition() {
		return this.position;
	}

	@Override
	public Proiettile shot() {
		return cannon.shot();
	}

	@Override
	public void update(InputImpl i) {
		this.setDirection(i.getDirection().get(), i.isPressed());
		updatePosition();
		this.updateCannon(i.getTargetPosition());
	}
	
	@Override
	public void damage(int damage) {
		this.lifes -= damage;
	}
	
	@Override
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	private void setDirection(Direction dir, boolean b) {
		if (dir == Direction.UP && b == true) {
			this.speedY = -speed;
		} else if (dir == Direction.UP && b == false) {
			this.speedY = 0;
		} else if (dir == Direction.DOWN && b == true) {
			this.speedY = speed;
		} else if (dir == Direction.DOWN && b == false) {
			this.speedY = 0;
		} else if (dir == Direction.RIGHT && b == true) {
			this.speedX = speed;
		} else if (dir == Direction.RIGHT && b == false) {
			this.speedX = 0;
		} else if (dir == Direction.LEFT && b == true) {
			this.speedX = -speed;
		} else if (dir == Direction.LEFT && b == false) {
			this.speedX = 0;
		}
	}

	private void updatePosition() {
		this.position = new Position2D(this.position.getX()+speedX, this.position.getY()+speedY);
	}
	
	private void updateCannon(Position2D target) {
		Position2D cannon_pos = new Position2D(this.position.getX()-2, this.position.getY()-2);
		this.cannon.update(cannon_pos, this.calculateAngle(cannon_pos, target));
	}
	
	
	private double calculateAngle(Position2D pos0, Position2D pos1) {
		double angle =  Math.toDegrees(Math.atan2(pos1.getY() - pos0.getY(), pos1.getX() - pos0.getX()));
		
		if(angle < 0){
			angle += 360;
		}
		
		return angle;
	}
	
	private class Cannon {
		private Position2D cannon_position;
		private double angle;
		
		public void update(Position2D position, double angle) {
			this.cannon_position = position;
			this.angle = angle;
		}

		public Proiettile shot() {
			return new Proiettile(cannon_position, this.angle);
		}
	}

}
