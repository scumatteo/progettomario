package model.input;

import model.utility.Position2D;

public interface CannonInput {
	Position2D getTargetPosition();
	void setTarget(Position2D target);
}
