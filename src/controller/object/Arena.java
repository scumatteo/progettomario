package controller.object;

import java.util.List;
import java.util.Optional;

import model.input.InputImpl;
import model.object.Proiettile;
import model.object.Tank;
import model.utility.Position2D;

public interface Arena {
	void createPlayer(Position2D position, double speed);
	void createEnemy(Position2D position, double speed);
	void setBorder(double h, double w);
	Optional<Tank> getPlayer();
	List<Optional<Tank>> getEnemies();
	List<Proiettile> getProiettile();
	void inputPlayer(InputImpl i);
	void inputEnemy(InputImpl i);
	void shotPlayer();
	void shotEnemy();
	void resetAll();
	void saveAll(String nameTxt);
}
