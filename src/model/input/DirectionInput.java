package model.input;

import java.util.Optional;

import model.utility.Direction;

public interface DirectionInput {
	void setDirection(Direction dir);
	Optional<Direction> getDirection();
	boolean isPressed();
}
