package controller.object;

import model.input.InputImpl;
import model.object.Tank;

public class ControllerTankImpl implements ControllerTank {
	
	private Tank playerTank;
	private Tank enemyTank;
	
	public ControllerTankImpl(Tank playerTank, Tank enemyTank) {
		this.playerTank = playerTank;
		this.enemyTank = enemyTank;
	}

	@Override
	public void updatePlayerTank() {
		
		//qui si fa la setOnKey, si setta la direction e si richiama la controllertank.updatePlayer
				//ma per farlo devo avere la scena, quindi istanzio una view e la prendo nel costruttore da 
				//application che istanzia tutto e ha il ciclo di gioco
	}

	@Override
	public void updateEnemyTank() {
	
		
	}


}
