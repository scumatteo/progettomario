package model.builder;

import model.object.Tank;
import model.object.TankImpl;
import model.utility.Position2D;

public class BuilderTank {
	private boolean isBuild=false;
	private Position2D position;
	private int lifes;
	private double speed;
	
	private BuilderTank() {
	}
	
	public BuilderTank createTank(){
		return this;
	}
	
	public BuilderTank setPosition(Position2D position) {
		this.position = position;
		return this;
	}
	
	public BuilderTank setLifes(int lifes) {
		if(lifes<=0) {
			throw new IllegalArgumentException();
		}
		this.lifes = lifes;
		return this;
	}
	
	public BuilderTank setSpeed(double speed) {
		if(speed<0) {
			throw new IllegalArgumentException();
		}
		this.speed = speed;
		return this;
	}
	
	public Tank build() {
		if(isBuild) {
			throw new IllegalStateException();
		}
		isBuild = true;
		return new TankImpl(position, lifes, speed);	
	}
}
