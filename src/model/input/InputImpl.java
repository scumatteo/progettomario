package model.input;

import java.util.Optional;

import model.utility.Direction;
import model.utility.Position2D;

public class InputImpl implements DirectionInput, CannonInput {
	
	private Optional<Direction> dir;
	private boolean isPressed=false;
	private Position2D target;
	
	@Override
	public Optional<Direction> getDirection() {
		return this.dir;
	}

	@Override
	public boolean isPressed() {
		return this.isPressed;
	}

	@Override
	public void setDirection(Direction dir) {
		Optional.ofNullable(dir);
		
	}

	@Override
	public Position2D getTargetPosition() {
		return this.target;
	}

	@Override
	public void setTarget(Position2D target) {
		this.target = target;
		
	}

}
