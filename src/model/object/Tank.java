package model.object;

import model.input.*;
import model.utility.Position2D;

public interface Tank {
	boolean isAlive();
	void setPosition(Position2D position);
	Position2D getPosition();
	Proiettile shot();
	void update(InputImpl i); 
	void setSpeed(double speed);
	void damage(int damage);
}
